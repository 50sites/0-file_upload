from app import app
from flask import request
from flask import send_from_directory
import os
from flask import Flask, request, redirect, url_for
from werkzeug.utils import secure_filename
import magic
from flask import render_template

UPLOAD_FOLDER = '/media/lex/Disk/50_sites/0-file_upload/app/img'
ALLOWED_EXTENSIONS = set(['txt', 'pdf', 'png', 'jpg', 'jpeg', 'gif'])


@app.route('/', methods=['GET', 'POST'])
def upload_file():
    if request.method == 'POST':
        file = request.files['file']
        if not file.filename.split('.')[1] in ALLOWED_EXTENSIONS:
            print('Недопустимый тип файла')
        filename = secure_filename(file.filename)
        file.save(os.path.join(UPLOAD_FOLDER, filename))

        return redirect(url_for('upload_file'))

    return render_template('index.html')